//
//  QuakerTests.m
//  QuakerTests
//
//  Created by Stu on 16/09/2014.
//  Copyright (c) 2014 ANZ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "Quake.h"
#import "DateFormatter.h"

@interface QuakerTests : XCTestCase

@end

@implementation QuakerTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testQuakeModelCreation {
    NSDictionary *quakeDict = @{
                                @"src": @"us",
                                @"eqid": @"c000is61",
                                @"timedate": @"2013-07-29 22:22:48",
                                @"lat": @"7.6413",
                                @"lon": @"93.6871",
                                @"magnitude": @"4.6",
                                @"depth": @"40.90",
                                @"region": @"Nicobar Islands, India region"};
    Quake *q = [[Quake alloc] initWithDictionary:quakeDict];
    NSDate *dateVal = [[[DateFormatter shared] jsonFormatter] dateFromString:@"2013-07-29 22:22:48"];
    XCTAssert([q.source isEqualToString:@"us"], @"Source property is set");
    XCTAssert([q.quakeId isEqualToString:@"c000is61"], @"QuakeId property is set");
    XCTAssert([q.timeDate isEqual:dateVal], @"TimeDate property is set");
    XCTAssert([q.latitude isEqualToNumber:[NSNumber numberWithFloat:7.6413]], @"Latitude property is set");
    XCTAssert([q.longitude isEqualToNumber:[NSNumber numberWithFloat:93.6871]], @"Longitude property is set");
    XCTAssert([q.magnitude isEqualToNumber:[NSNumber numberWithFloat:4.6]], @"Magnitude property is set");
    XCTAssert([q.depth isEqualToNumber:[NSNumber numberWithFloat:40.90]], @"Depth property is set");
    XCTAssert([q.region isEqualToString:@"Nicobar Islands, India region"], @"Region property is set");
    
}

- (void)testDateFormatting {
    NSDate *convertedDate = [[[DateFormatter shared] jsonFormatter] dateFromString:@"2013-07-29 22:22:48"];
    XCTAssert(convertedDate, @"JSON Date formatting failed");
    
    NSString *formattedDate = [[[DateFormatter shared] labelFormatter] stringFromDate:convertedDate];
    XCTAssert([formattedDate isEqualToString:@"29/07/2013"], @"Cell Date formatting failed");
}

- (void)testExample {
    // This is an example of a functional test case.
    XCTAssert(YES, @"Pass");
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
