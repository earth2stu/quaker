//
//  ANDateFormatter.h
//  ABCNews
//
//  Created by Stu on 23/08/2014.
//  Copyright (c) 2014 b2cloud. All rights reserved.
//

#import <Foundation/Foundation.h>

// use static date formatters to improve performance
@interface DateFormatter : NSDateFormatter

/**
 *  Singleton instance of date formatter
 */
+ (DateFormatter*)shared;

/**
 *  Used to convert from date given in json feed
 */
- (NSDateFormatter*)jsonFormatter;

/**
 *  Used to convert to display date for from and to date labels
 */
- (NSDateFormatter*)labelFormatter;

/**
 *  Used to convert date for main label
 */
- (NSDateFormatter *)mainLabelFormatter;


@end
