//
//  AppDelegate.h
//  Quaker
//
//  Created by Stu on 16/09/2014.
//  Copyright (c) 2014 ANZ. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 *  Commone Application Delegate
 */
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

