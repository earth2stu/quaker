//
//  ViewController.m
//  Quaker
//
//  Created by Stu on 16/09/2014.
//  Copyright (c) 2014 ANZ. All rights reserved.
//

#import "ViewController.h"
#import "DataManager.h"
#import "Constants.h"
#import "Quake.h"
#import "SVProgressHUD.h"
#import "DateFormatter.h"
#import "Quake+MKAnnotation.h"
#import "QuakeAnnotationView.h"

static const float kAnimationDuration = 0.4f;

@interface ViewController ()

@property (nonatomic, strong) NSArray *quakes;
@property (nonatomic, strong) NSDate *fromDate;
@property (nonatomic, strong) NSDate *toDate;
@property (assign) BOOL isPlaying;
@property (assign) BOOL isMenuHidden;
@property (assign) BOOL isCalloutOpen;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // load the cached data if we have it
    // otherwise get from the network
    [self loadDataWithCacheStrategy:CacheStrategyCacheElseNetwork];
    
    [self.progressSlider addTarget:self action:@selector(sliderDidTouchUp:) forControlEvents:UIControlEventTouchUpInside];
    [self.progressSlider addTarget:self action:@selector(sliderDidTouchDown:) forControlEvents:UIControlEventTouchDown];
    [self.progressSlider addTarget:self action:@selector(sliderDidChange:) forControlEvents:UIControlEventValueChanged];
    
    UITapGestureRecognizer *tgr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(touchMap:)];
    [self.quakeMapView addGestureRecognizer:tgr];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadDataWithCacheStrategy:(CacheStrategy)strategy {
    NSURL *dataURL = [NSURL URLWithString:kAPIUrl];
    
    [SVProgressHUD show];
    [self enableControls:NO];
    [[DataManager sharedManager] loadItemsAtURL:dataURL
                                        ofClass:[Quake class]
                                   fromRootNode:@"earthquakes"
                                   withStrategy:strategy
                                 withCompletion:^(NSArray *items, BOOL success) {
                                     if (success) {
                                         self.quakes = items;
                                         [SVProgressHUD showSuccessWithStatus:@"Data updated"];
                                         [self resetQuakes];
                                     } else {
                                         [SVProgressHUD showErrorWithStatus:@"Data update failed"];
                                     }
                                     
                                     
                                     [self enableControls:YES];
                                 }];
}

- (void)enableControls:(BOOL)enabled {
    self.playPauseButton.enabled = enabled;
    self.refreshButton.enabled = enabled;
    self.progressSlider.enabled = enabled;
    
}

- (void)resetQuakes {
    self.progressSlider.value = 0.0;
    
    // get the dates for first and last quakes
    Quake *firstQuake = [self.quakes lastObject];
    Quake *lastQuake = [self.quakes firstObject];
    self.fromDate = firstQuake.timeDate;
    self.toDate = lastQuake.timeDate;
    
    // show the dates on the labels
    self.fromDateLabel.text = [[[DateFormatter shared] labelFormatter] stringFromDate:firstQuake.timeDate];
    self.toDateLabel.text = [[[DateFormatter shared] labelFormatter] stringFromDate:lastQuake.timeDate];
    self.mainDateLabel.text = [[[DateFormatter shared] mainLabelFormatter] stringFromDate:[self dateForPercentage:self.progressSlider.value]];
    
    [self showCurrentQuakes];
    [self.quakeMapView showAnnotations:self.quakeMapView.annotations animated:YES];
}


#pragma mark Date Range Management

- (NSTimeInterval)secondsRange {
    return [self.toDate timeIntervalSinceDate:self.fromDate];
}

- (NSDate*)dateForPercentage:(float)percentage {
    return [NSDate dateWithTimeInterval:[self secondsRange] * percentage sinceDate:self.fromDate];
}

- (NSArray*)currentQuakes {
    NSMutableArray *tempQuakes = [NSMutableArray array];
    NSDate *currentSliderDate = [self dateForPercentage:self.progressSlider.value];
    float dayRange = 24 * 60 * 60;
    for (Quake *quake in self.quakes) {
        if ( fabs([quake.timeDate timeIntervalSinceDate:currentSliderDate]) < dayRange ) {
            [tempQuakes addObject:quake];
        }
    }
    return [NSArray arrayWithArray:tempQuakes];
}

#pragma mark Slider

- (void)sliderDidTouchUp:(UISlider*)slider {
    NSLog(@"up %@", slider);
}

- (void)sliderDidTouchDown:(UISlider*)slider {
    NSLog(@"down %@", slider);
}

- (void)sliderDidChange:(UISlider*)slider {
    self.mainDateLabel.text = [[[DateFormatter shared] mainLabelFormatter] stringFromDate:[self dateForPercentage:slider.value]];
    [self showCurrentQuakes];
}

#pragma mark Menu Hide and Show

- (void)toggleMenu {
    if (self.isMenuHidden) {
        [self showMenu];
    } else {
        [self hideMenu];
    }
}

- (void)hideMenu {
    self.isMenuHidden = YES;
    [UIView animateWithDuration:kAnimationDuration animations:^{
        self.menuView.alpha = 0.0f;
    }];
}

- (void)showMenu {
    self.isMenuHidden = NO;
    [UIView animateWithDuration:kAnimationDuration animations:^{
        self.menuView.alpha = 0.6f;
    }];
}

#pragma mark Map Management

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
    
    QuakeAnnotationView *quakeView = (QuakeAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:@"Quake"];
    
    if (!quakeView) {
        quakeView = [[QuakeAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"Quake"];
    } else {
        quakeView.annotation = annotation;
    }
    quakeView.canShowCallout = YES;
    [quakeView resetAnimation];
    //quakeView.image = [UIImage imageNamed:@"circle_gradient1.png"];
    return quakeView;
    
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view {
    self.isCalloutOpen = YES;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.isCalloutOpen = NO;
    });
}

- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view {
    
}

- (void)showCurrentQuakes {
    
    NSSet *currentAnnotations = [NSSet setWithArray:self.quakeMapView.annotations];
    NSMutableSet *addedAnnotations = [NSMutableSet setWithArray:[self currentQuakes]];
    
    NSMutableSet *annotationsToKeep = [NSMutableSet setWithSet:currentAnnotations];
    [annotationsToKeep intersectSet:addedAnnotations];
    
    NSMutableSet *annotationsToRemove = [NSMutableSet setWithSet:currentAnnotations];
    [annotationsToRemove minusSet:annotationsToKeep];
    
    [self.quakeMapView removeAnnotations:[annotationsToRemove allObjects]];
    
    [addedAnnotations minusSet:annotationsToKeep];
    
    
    [self.quakeMapView addAnnotations:[addedAnnotations allObjects]];
    
}

#pragma mark Button Handling

- (void)touchMap:(UITapGestureRecognizer*)recognizer {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (!self.isCalloutOpen) {
            [self toggleMenu];
        }
        
    });
    
}

- (IBAction)touchRefresh:(id)sender {
    [self loadDataWithCacheStrategy:CacheStrategyNetworkOnly];
}

- (IBAction)touchPlay:(id)sender {
    if (self.isPlaying) {
        self.isPlaying = NO;
        self.progressSlider.value = 0.0f;
        self.playPauseButton.selected = NO;
        self.mainDateLabel.text = [[[DateFormatter shared] mainLabelFormatter] stringFromDate:[self dateForPercentage:self.progressSlider.value]];
    } else {
        self.isPlaying = YES;
        [self playbackTick];
        self.playPauseButton.selected = YES;
    }
}

#pragma mark Playback Handling

- (void)playbackTick {
    if (!self.isPlaying) {
        return;
    }
    if (self.progressSlider.value == 1.0f) {
        [self touchPlay:nil];
        return;
    }
    [self showCurrentQuakes];
    self.progressSlider.value = self.progressSlider.value + kPlaybackTick / kPlaybackDuration;
    self.mainDateLabel.text = [[[DateFormatter shared] mainLabelFormatter] stringFromDate:[self dateForPercentage:self.progressSlider.value]];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(kPlaybackTick * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self playbackTick];
    });
    
}

@end
