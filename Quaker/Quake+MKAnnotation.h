//
//  Quake+MKAnnotation.h
//  Quaker
//
//  Created by Stu on 17/09/2014.
//  Copyright (c) 2014 ANZ. All rights reserved.
//

#import "Quake.h"
#import <MapKit/MapKit.h>

/**
 *  Category on Quake object conforming to MKAnnotation
 *  Allows it to be added as notation to map
 */
@interface Quake (MKAnnotation) <MKAnnotation>

@end
