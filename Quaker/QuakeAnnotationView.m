//
//  QuakeAnnotationView.m
//  Quaker
//
//  Created by Stu on 17/09/2014.
//  Copyright (c) 2014 ANZ. All rights reserved.
//

#import "QuakeAnnotationView.h"
#import "Quake.h"

@interface QuakeAnnotationView() {
    UIView *circleView;
}

@end

@implementation QuakeAnnotationView

- (instancetype)initWithAnnotation:(id<MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
    if (self) {
        
        UIView *centreCircleView = [[UIView alloc] initWithFrame:CGRectMake(-5, -5, 10, 10)];
        centreCircleView.backgroundColor = [UIColor redColor];
        centreCircleView.alpha = 0.5f;
        centreCircleView.layer.cornerRadius = 2.0;
        [self addSubview:centreCircleView];
        
        UIView *touchableView = [[UIView alloc] initWithFrame:CGRectMake(-10, -10, 20, 20)];
        touchableView.backgroundColor = [UIColor clearColor];
        touchableView.layer.cornerRadius = 5.0;
        [self addSubview:touchableView];
        
        circleView = [[UIView alloc] initWithFrame:CGRectMake(-10, -10, 20, 20)];
        circleView.backgroundColor = [UIColor whiteColor];
        circleView.alpha = 0.5f;
        circleView.layer.cornerRadius = 3.0;
        [self addSubview:circleView];
        
        
    }
    return self;
}


- (void)resetAnimation {
    circleView.alpha = 1.0f;
    circleView.transform = CGAffineTransformMakeScale(0.2, 0.2);
    Quake *quake = (Quake*)self.annotation;
    // convert richter scale to linear
    float visualMagnitude = powf([quake.magnitude floatValue], 10.0) / 10000000.0;
    [UIView animateWithDuration:1.0 animations:^{
        circleView.alpha = 0.0f;
        circleView.transform = CGAffineTransformMakeScale(visualMagnitude, visualMagnitude);
    }];
    
}

@end
