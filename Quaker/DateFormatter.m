//
//  ANDateFormatter.m
//  ABCNews
//
//  Created by Stu on 23/08/2014.
//  Copyright (c) 2014 b2cloud. All rights reserved.
//

#import "DateFormatter.h"

static NSDateFormatter *_jsonFormatter;
static NSDateFormatter *_labelFormatter;
static NSDateFormatter *_mainLabelFormatter;
static NSDateFormatter *_calloutFormatter;

@implementation DateFormatter

+ (DateFormatter *)shared {
    static DateFormatter *_shared = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _shared = [[self alloc] init];
    });
    return _shared;
}

- (NSDateFormatter *)jsonFormatter {
    if (!_jsonFormatter) {
        _jsonFormatter = [[NSDateFormatter alloc] init];
        _jsonFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    }
    return _jsonFormatter;
}

- (NSDateFormatter *)labelFormatter {
    if (!_labelFormatter) {
        _labelFormatter = [[NSDateFormatter alloc] init];
        _labelFormatter.dateFormat = @"dd/MM/yyyy";
    }
    return _labelFormatter;
}

- (NSDateFormatter *)mainLabelFormatter {
    if (!_mainLabelFormatter) {
        _mainLabelFormatter = [[NSDateFormatter alloc] init];
        _mainLabelFormatter.dateFormat = @"dd/MM/yyyy";
    }
    return _mainLabelFormatter;
}

@end
