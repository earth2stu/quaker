//
//  Quake.m
//  Quaker
//
//  Created by Stu on 16/09/2014.
//  Copyright (c) 2014 ANZ. All rights reserved.
//

#import "Quake.h"
#import "DateFormatter.h"

@implementation Quake

- (id)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    if (self) {
        self.source = dict[@"src"];
        self.quakeId = dict[@"eqid"];
        NSString *timeDateVal = dict[@"timedate"];
        self.timeDate = [[[DateFormatter shared] jsonFormatter] dateFromString:timeDateVal];
        self.latitude = [NSNumber numberWithFloat:[dict[@"lat"] floatValue]];
        self.longitude = [NSNumber numberWithFloat:[dict[@"lon"] floatValue]];
        self.magnitude = [NSNumber numberWithFloat:[dict[@"magnitude"] floatValue]];
        self.depth = [NSNumber numberWithFloat:[dict[@"depth"] floatValue]];
        self.region = dict[@"region"];
    }
    return self;
}


@end
