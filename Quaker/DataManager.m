//
//  DataManager.m
//  Quaker
//
//  Created by Stu on 16/09/2014.
//  Copyright (c) 2014 ANZ. All rights reserved.
//

#import "DataManager.h"

@implementation DataManager

+ (DataManager*)sharedManager {
    static DataManager *_sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedManager = [[self alloc] init];
    });
    return _sharedManager;
}

- (void)loadItemsAtURL:(NSURL *)url
               ofClass:(Class)itemClass
          fromRootNode:(NSString *)rootNode
          withStrategy:(CacheStrategy)strategy
        withCompletion:(void (^)(NSArray *, BOOL))completion {
    
    
    NSData *fileData = [NSData dataWithContentsOfURL:[self fileCacheURL]];
    if (strategy == CacheStrategyCacheOnly || strategy == CacheStrategyCacheElseNetwork) {
        // check for cached data
        if (fileData) {
            // return cache if found
            NSArray *items = [self itemsWithData:fileData ofClass:itemClass fromRootNode:rootNode];
            completion(items, YES);
            return;
        }
    }
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSession *session = [NSURLSession sharedSession];
    
    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        //if (!error) {
            
            
            NSURL *jsonURL = [[NSBundle mainBundle] URLForResource:@"quakes" withExtension:@"json"];
            data = [NSData dataWithContentsOfURL:jsonURL];
            // cache the data
            [data writeToURL:[self fileCacheURL] atomically:YES];
            NSArray *articles = [self itemsWithData:data ofClass:itemClass fromRootNode:rootNode];
            // call on main thread cos we are in the background
            dispatch_async(dispatch_get_main_queue(), ^{
                completion(articles, YES);
            });
        /*
        } else {
            // if there was an error try and use the cache anyway
            if (fileData) {
                NSArray *articles = [self itemsWithData:fileData ofClass:itemClass fromRootNode:rootNode];
                // call on main thread cos we are in the background
                dispatch_async(dispatch_get_main_queue(), ^{
                    completion(articles, NO);
                });
            } else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    completion(nil, NO);
                });
            }
        }
         */
    }] resume];
    
}


// parse json data into model objects
// returns nil if an error occured
// root node must contain a json array of objects
// class must have an initWithDictionary selector
- (NSArray*)itemsWithData:(NSData*)data ofClass:(Class)itemClass fromRootNode:(NSString*)rootNode {
    NSError *jsonError = nil;
    NSDictionary *jsonObject = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&jsonError];
    if (!jsonError) {
        // browse the json object
        NSArray *baseArray = [jsonObject objectForKey:rootNode];
        NSMutableArray *items = [NSMutableArray array];
        // add all the articles
        for (NSDictionary *itemDict in baseArray) {
            if ([itemClass instancesRespondToSelector:@selector(initWithDictionary:)]) {
                id item = [[itemClass alloc] initWithDictionary:itemDict];
                [items addObject:item];
            }
        }
        return [NSArray arrayWithArray:items];
    } else {
        return nil;
    }
}



- (NSURL*)fileCacheURL {
    NSString *cachesPath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) firstObject];
    NSString *filePath = [cachesPath stringByAppendingPathComponent:@"quakes.json"];
    return [NSURL fileURLWithPath:filePath];
}

@end
