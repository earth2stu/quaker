//
//  Quake.h
//  Quaker
//
//  Created by Stu on 16/09/2014.
//  Copyright (c) 2014 ANZ. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  Data model representing an earthquake event
 */
@interface Quake : NSObject

/**
 *  The source of the earthquake in the data
 */
@property (nonatomic, strong) NSString *source;

/**
 *  Unique identifier of earthquake
 */
@property (nonatomic, strong) NSString *quakeId;

/**
 *  Timestamp of the earthquake
 */
@property (nonatomic, strong) NSDate *timeDate;

/**
 *  Latitude of the earthquake
 */
@property (nonatomic, strong) NSNumber *latitude;

/**
 *  Longitude of the earthquake
 */
@property (nonatomic, strong) NSNumber *longitude;

/**
 *  Magnitude of the earthquake
 */
@property (nonatomic, strong) NSNumber *magnitude;

/**
 *  Depth of the earthquake
 */
@property (nonatomic, strong) NSNumber *depth;

/**
 *  String description of region of the earthquake
 */
@property (nonatomic, strong) NSString *region;

/**
 *  Initialise the quake model with data from json
 */
- (id)initWithDictionary:(NSDictionary*)dict;

@end
