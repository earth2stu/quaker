//
//  main.m
//  Quaker
//
//  Created by Stu on 16/09/2014.
//  Copyright (c) 2014 ANZ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
