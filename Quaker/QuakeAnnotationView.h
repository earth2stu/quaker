//
//  QuakeAnnotationView.h
//  Quaker
//
//  Created by Stu on 17/09/2014.
//  Copyright (c) 2014 ANZ. All rights reserved.
//

#import <MapKit/MapKit.h>

/**
 *  Animated annotation view to illustrate quake magnitude
 */
@interface QuakeAnnotationView : MKAnnotationView

/**
 *  Reset to origins and start playing quake magnitude animation
 */
- (void)resetAnimation;

@end
