//
//  Constants.h
//  Quaker
//
//  Created by Stu on 16/09/2014.
//  Copyright (c) 2014 ANZ. All rights reserved.
//

/**
 *  URL for earthquake data API
 */
extern NSString *const kAPIUrl;

/**
 *  Duration of earthquake animation playback
 */
extern float const kPlaybackDuration;

/**
 *  Length of playback frame time
 */
extern float const kPlaybackTick;

