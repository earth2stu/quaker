//
//  ViewController.h
//  Quaker
//
//  Created by Stu on 16/09/2014.
//  Copyright (c) 2014 ANZ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

/**
 *  Main view controller for UI
 */
@interface ViewController : UIViewController <MKMapViewDelegate>

/**
 *  Interactive progress slider
 */
@property (retain, nonatomic) IBOutlet UISlider *progressSlider;

/**
 *  Label showing current from date
 */
@property (retain, nonatomic) IBOutlet UILabel *fromDateLabel;

/**
 *  Label showing current to date
 */
@property (retain, nonatomic) IBOutlet UILabel *toDateLabel;

/**
 *  Main map view
 */
@property (retain, nonatomic) IBOutlet MKMapView *quakeMapView;

/**
 *  Centre date label showing current date
 */
@property (retain, nonatomic) IBOutlet UILabel *mainDateLabel;

/**
 *  Refresh data button
 */
@property (retain, nonatomic) IBOutlet UIButton *refreshButton;

/**
 *  Play / pause button
 */
@property (retain, nonatomic) IBOutlet UIButton *playPauseButton;

/**
 *  Menu container view
 */
@property (retain, nonatomic) IBOutlet UIView *menuView;

@end

