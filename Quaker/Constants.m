//
//  Constants.m
//  Quaker
//
//  Created by Stu on 16/09/2014.
//  Copyright (c) 2014 ANZ. All rights reserved.
//

#import <Foundation/Foundation.h>

NSString *const kAPIUrl = @"http://www.seismi.org/api/eqs/";
float const kPlaybackDuration = 15.0f;
float const kPlaybackTick = 1.0f / 60.0f;
