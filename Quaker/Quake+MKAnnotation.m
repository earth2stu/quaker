//
//  Quake+MKAnnotation.m
//  Quaker
//
//  Created by Stu on 17/09/2014.
//  Copyright (c) 2014 ANZ. All rights reserved.
//

#import "Quake+MKAnnotation.h"
#import "DateFormatter.h"

@implementation Quake (MKAnnotation)

- (CLLocationCoordinate2D)coordinate {
    return CLLocationCoordinate2DMake([self.latitude floatValue], [self.longitude floatValue]);
}

- (NSString *)title {
    return [[[DateFormatter shared] labelFormatter] stringFromDate:self.timeDate];
}

- (NSString *)subtitle {
    return [NSString stringWithFormat:@"Magnitude: %@", self.magnitude];
}

@end
