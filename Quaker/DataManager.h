//
//  DataManager.h
//  Quaker
//
//  Created by Stu on 16/09/2014.
//  Copyright (c) 2014 ANZ. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  Cache strategy types
 */
typedef enum : NSUInteger {
    CacheStrategyCacheOnly,             // only try loading data from cache
    CacheStrategyCacheElseNetwork,      // load from cache if available, otherwise network
    CacheStrategyNetworkOnly            // skip the cache and load only from network
} CacheStrategy;

/**
 *  General data manager
 *  Singleton responsible for loading data from network or local cache
 */
@interface DataManager : NSObject

/**
 *  Singleton instance
 */
+ (DataManager*)sharedManager;

/**
 *  Load items at the given url
 *  @param url - url to load data from
 *  @param itemClass - target class for conversion of json dictionaries
 *  @param rootNode - 
 *  @param strategy - the caching strategy used to load data
 *  @param completion - the completion block called to return items and success flag
 */
- (void)loadItemsAtURL:(NSURL*)url
               ofClass:(Class)itemClass
          fromRootNode:(NSString*)rootNode
          withStrategy:(CacheStrategy)strategy
        withCompletion:(void (^)(NSArray *items, BOOL success))completion;


@end
